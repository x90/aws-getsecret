# aws-getsecret

Simplistic program to fetch secrets from AWS Secrets Manager and output the secret string to stdout.

## Usage

`./getsecret us-east-1 NameOfSecret`

First argument required is AWS Region, second is the secret name.

## Build instructions

`make all`
