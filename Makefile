all: linux windows

CMD=go build -tags netgo

prep:
	go get -d -v

linux: aws-getsecret.go | prep
	GOARCH=amd64 GOOS=linux $(CMD) -o getsecret -ldflags '-s -w -extldflags "-static -fno-PIC"' -buildmode pie aws-getsecret.go

windows: aws-getsecret.go | prep
	GOARCH=amd64 GOOS=windows $(CMD) -o getsecret.exe aws-getsecret.go
