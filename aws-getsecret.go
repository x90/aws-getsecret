package main

import (
	"os"
	"fmt"
	"log"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/secretsmanager"
)

func main() {
	region := os.Getenv("AWS_REGION")
	if region == "" {
		log.Fatal("[!] No AWS_REGION environment variable, unable to proceed")
	}

	if len(os.Args) != 2 {
		log.Fatal("[!] Expected 1 command-line argument, unable to proceed")
	}

	secretName := os.Args[1]

	sess := session.Must(session.NewSession(&aws.Config{
		Region: aws.String(region),
	}))

	svc := secretsmanager.New(sess)

	input := &secretsmanager.GetSecretValueInput{
		SecretId: aws.String(secretName),
	}

	result, err := svc.GetSecretValue(input)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println(*result.SecretString)
}
